<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropRoleUserColumnToMstrUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mstr_users', function (Blueprint $table) {
            $table->dropColumn('role_user');
        });
        Schema::table('mstr_users', function (Blueprint $table) {
            $table->string('role_user')->after('password')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('mstr_users', function (Blueprint $table) {
            //
        });
    }
}
