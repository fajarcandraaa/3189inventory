<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoreoutTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('storeout', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('notrans');
            $table->string('brand');
            $table->string('kodebarcode');
            $table->integer('qty');
            $table->string('totalprice');
            $table->string('cashier');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('storeout');
    }
}
