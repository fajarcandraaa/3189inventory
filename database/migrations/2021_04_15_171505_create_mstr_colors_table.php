<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMstrColorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mstr_colors', function (Blueprint $table) {
            $table->id();
            $table->string('colors_name');
            $table->string('colors_code');
            $table->string('colors_description')->nullable();
            $table->string('colors_initials')->nullable();
            $table->string('colors_companiesgroup')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mstr_colors');
    }
}
