<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('brand');
            $table->string('kodebarcode');
            $table->string('item_category');
            $table->string('item_color')->nullable();
            $table->integer('qty');
            $table->string('hpp')->nullable();
            $table->string('price');
            $table->string('totalasset');
            $table->string('item_imagesrc')->nullable();
            $table->softDeletes();
            $table->timestamps();});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory');
    }
}
