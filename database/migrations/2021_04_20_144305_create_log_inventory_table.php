<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogInventoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_inventory', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('kodebarang1');
            $table->string('kodebarang2');
            $table->string('kodebarcode');
            $table->string('invoice_no')->nullable();
            $table->string('title')->nullable();
            $table->string('item_size');
            $table->integer('qty');
            $table->string('price');
            $table->string('stock_akhir')->nullable();
            $table->string('item_status')->nullable();
            $table->string('total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_inventory');
    }
}
