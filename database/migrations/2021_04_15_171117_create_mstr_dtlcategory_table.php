<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMstrDtlcategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mstr_dtlcategory', function (Blueprint $table) {
            $table->id();
            $table->string('dtlcat_name');
            $table->string('dtlcat_code');
            $table->string('dtlcat_description')->nullable();
            $table->string('dtlcat_companiesgroup')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mstr_dtlcategory');
    }
}
