<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'auth', 'middleware' => 'cors'], function () use ($router) {
    $router->post('login', 'AuthController@authlogin');
});

$router->group(['middleware' => 'jwt.auth'], function () use ($router) {
    //user management
    $router->post('users','UsersController@registernewuser');
    $router->get('users[/{id:[0-9]+}]', 'UsersController@getallusers');
    $router->put('users/{id:[0-9]+}', 'UsersController@updateusers');
    $router->delete('users/{id:[0-9]+}', 'UsersController@deleteusers');
    
    //companies management
    $router->post('companies','CompaniesController@addnewcompany');
    $router->get('companies[/{id:[0-9]+}]', 'CompaniesController@getcompanies');
    $router->put('companies/{id:[0-9]+}', 'CompaniesController@updatecompanies');
    $router->delete('companies/{id:[0-9]+}', 'CompaniesController@deletecompanies');
    
    //role management
    $router->post('roles','RolesController@addnewroles');
    $router->get('roles', 'RolesController@getallroles');
    $router->put('roles/{id:[0-9]+}', 'RolesController@updateroles');
    $router->delete('roles/{id:[0-9]+}', 'RolesController@deleteroles');

    //brand management
    $router->post('brand','BrandsController@addnewbrand');
    $router->get('brand[/{id:[0-9]+}]', 'BrandsController@getallbrands');
    $router->get('brandbycomp', 'BrandsController@getallbrandsbycompanies');
    $router->put('brand/{id:[0-9]+}', 'BrandsController@updatebranddata');
    $router->delete('brand/{id:[0-9]+}', 'BrandsController@deletebrand');
    
    //category management
    $router->post('categories','CategoryController@registercategory');
    $router->get('categories', 'CategoryController@getallcategory');
    $router->put('categories/{id:[0-9]+}', 'CategoryController@updatecategory');
    $router->delete('categories/{id:[0-9]+}', 'CategoryController@deletecategory');
    //chid category management
    $router->post('dtlcategories','CategoryController@addchilditem');
    $router->get('dtlcategories', 'CategoryController@getallchildcategory');
    $router->put('dtlcategories/{id:[0-9]+}', 'CategoryController@udpatedtlcategory');
    $router->delete('dtlcategories/{id:[0-9]+}', 'CategoryController@deletedtlcategory');
    
    //color management
    $router->post('color','ColorsController@addnewcolor');
    $router->get('color', 'ColorsController@getallcolor');
    $router->delete('color/{id:[0-9]+}', 'ColorsController@deletecolor');
    
    //size management
    $router->post('size','SizeController@addnewsize');
    $router->get('size', 'SizeController@getallsize');
    $router->get('sizecategory', 'SizeController@getsizebycategory');
    $router->delete('size/{id:[0-9]+}', 'SizeController@deletesize');
    
    //inventory
    $router->post('inventory','InventoryController@batchinsert');
    $router->get('generatecode','InventoryController@generatekodebarang');
    $router->get('inventorygetbarcode','InventoryController@getkodebarcodebybrand');
    $router->get('inventory','InventoryController@cekinventory');
    $router->post('inventoryupload','InventoryController@uploadimagecatalog');
    $router->post('changeinvenimage','InventoryController@changeimage');
    // $router->post('inventory','InventoryController@reciveitem');
    // $router->post('batchinventory','InventoryController@batchinsert');

    //Storeout
    $router->post('storeout','StoreoutController@sotrout');
    $router->get('ourstock','StoreoutController@getstock');

});