<?php
namespace App\Http\Middleware;
use Closure;
use Exception;
use App\UsersModel;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
class JwtMiddleware
{
    public function handle($request, Closure $next, $guard = null)
    {
        $token = $request->header('token');

        if ($token == null) {
            return response()->json(['message' => 'Please cek your token verification'], 400);
        } else {   
            try {
                if(!$token) {
                    return response()->json([
                        'error' => 'Token not provided.'
                    ], 401);
                }
                try {
                    $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
                } catch(ExpiredException $e) {
                    return response()->json([
                        'error' => 'Provided token is expired.'
                    ], 400);
                } catch(Exception $e) {
                    return response()->json([
                        'error' => 'An error while decoding token.'
                    ], 400);
                }
                $user = UsersModel::find($credentials->usr);
                
            } catch(ExpiredException $e) {
                return response()->json([
                    'error' => 'Provided token is expired.'
                ], 400);
            } catch(Exception $e) {
                return response()->json([
                    'error' => 'An error while decoding token.'
                ], 400);
            }
            $request->auth  = $user;   
            return $next($request);
        }
    }
}