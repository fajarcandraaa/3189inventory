<?php
namespace App\Http\Controllers;

use App\UsersModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    private $request;
    
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    //generate jwt token
    protected function jwt(UsersModel $user)
    {
        $payload = [
            'iss' => "3189InventUser", //issuer 
            'sub' => "access-token", //subject
            'app' => env('APP_KEY'), //custom data 1
            'usr' => $user->id, //cutom data 2
            'iat' => time(),  // time when jwt was issued
            'exp' => time() + 60 * 60 * 24, //expiration time //1 hour
        ];
        return JWT::encode($payload, env('JWT_SECRET'));
    }

    public function authlogin(UsersModel $user)
    {
        date_default_timezone_set("Asia/Bangkok");
        $this->validate($this->request, [
            'email'     => 'string',
            'password'  => 'required'
        ]);

        $user   = UsersModel::where('email',$this->request->input('email'))
                    ->orWhere('username',$this->request->input('email'))
                    ->first();

        if (!$user) {
            return response()->json([
                'message'   => 'User does not exist'
            ], 400);
        } else {
            if (Hash::check($this->request->input('password'), $user->password)) {
                return response()->json([
                    'token'             => $this->jwt($user),
                    'id'                => $user->id,
                    'companiesGroup'    => $user->companiesgroup_user,
                    'name'              => $user->name,
                    'email'             => $user->email,
                    'role_user'         => $user->role_user
                ], 200);
            } else {
                // Bad Request response
                return response()->json([
                    'message' => 'Wrong username or password.'
                ], 400);
            }
        }
    }
}