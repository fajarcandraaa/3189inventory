<?php
namespace App\Http\Controllers;

use App\CategoryModel;
use App\ItemCategoryModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class CategoryController extends Controller
{
    //CRUD CATEGORY
    public function registercategory(Request $request)
    {
        $this->validate($request, [
            'category_name'         => 'required|string',
            'category_code'         => 'required|string',
            'category_description'  => 'string',
            'group_item'            => 'required|string'
        ]);

        try {
            $category   = new CategoryModel;
            $params     = $request->all();
            $category->fill($params);
            
            $cekcategory    = $category->where('category_name', $request->input('category_name'))
                                ->where('category_code', $request->input('category_code'))
                                ->first();
            // return $cekcategory;
            if ($cekcategory) {
                return response()->json(['message' => 'This item category already exist!'], 409);
            } else {
                $category->save();
                return response()->json(['category' => $category, 'message' => 'CREATED'], 201);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'Item category Registration Failed!', 'error_report' => $e], 409);
        }
    }

    public function getallcategory(Request $request)
    {
        $status         = false;
        $error          = "data not found";
        $getcategory    = new CategoryModel;

        if ($getcategory->get()) {
            $status         = true;
            $error          = null;
            $limit          = $request->has('limit') ? $request->input('limit') : 20;
            $page           = $request->has('page') ? $request->input('page') : 1;
            $getcategory   = $getcategory->paginate($limit,['*'],'page',$page);
            $meta           = [
                'page'      => (int) $getcategory->currentPage(),
                'perPage'   => (int) $getcategory->perPage(),
                'total'     => (int) $getcategory->total(),
                'totalPage' => (int) $getcategory->lastPage()
            ];
            $getcategory   = $getcategory->toArray()['data'];
        }

        $response   = [
            'status'        => (bool) $status,
            'data'          => (isset($getcategory) ? $getcategory : null),
            'meta'          => (isset($meta) ? $meta : null),
            'error'         => (isset($error) ? $error : null)
        ];
        return response()->json($response);
    }

    public function updatecategory(Request $request, $id)
    {
        $this->validate($request,[
            'category_name'         => 'required|string',
            'category_code'         => 'required|string'
        ]);

        $datacategory  = CategoryModel::find($id);
        
        if ($datacategory != null) {
            $params         = $request->all();
            $cekcategory    = CategoryModel::where(function ($query) use($request) {
                                $query->where('category_name', $request->input('category_name'))
                                ->orWhere('category_code', $request->input('category_code'));
                            })->where('id','!=',$id);

            $cekcategory   = $cekcategory->first();
            
            if ($cekcategory) {
                return response()->json(['message' => "Category's Name or Code already exist!"], 409);
            } else {
                $datacategory->fill($params);
                $datacategory->save();
                return response()->json(['status' => (bool) true ,'message' => 'Your data has been update'], 200);
            }    
        }
    }

    public function deletecategory(Request $request, $id)
    {
        $category       = CategoryModel::find($id);
        if ($category) {
            $category->delete();
            return response()->json(['status' => (bool) true], 200);
        } else {
            return response()->json(['status' => (bool) false], 409);
        }
    }

    //CRUD CHILD CATEGORY
    public function addchilditem(Request $request)
    {
        $this->validate($request, [
            'dtlcat_name'           => 'required|string',
            'dtlcat_code'           => 'required|string',
            'parentcode'            => 'required|string'
        ]);

        try {
            $dtlcategory    = new ItemCategoryModel;
            $request['dtlcat_companiesgroup']   = $request->auth->companiesgroup_user;
            $params         = $request->all();
            $dtlcategory->fill($params);
            $cekdtlcategory     = $dtlcategory->where('dtlcat_name', $request->input('dtlcat_name'))
                                    ->where('dtlcat_code', $request->input('dtlcat_code'))
                                    ->where('dtlcat_companiesgroup', $request->auth->companiesgroup_user)
                                    ->first();

            if ($cekdtlcategory) {
                return response()->json(['message' => 'This item category already exist!'], 409);
            } else {
                $dtlcategory->save();
                return response()->json(['dtl_category' => $dtlcategory, 'message' => 'CREATED'], 201);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'Item category Registration Failed!', 'error_report' => $e], 409);
        }
    }

    public function getallchildcategory(Request $request)
    {
        $status         = false;
        $error          = "data not found";
        $getdtlcat      = ItemCategoryModel::with(['dtlcat_companiesgroup']);

        if ($request->has('parentcode')) {
            $getdtlcat = $getdtlcat->where('parentcode',$request->input('parentcode'));
        }

        try {
            if ($getdtlcat->get()) {
                $status     = true;
                $error      = null;
                $limit      = $request->has('limit') ? $request->input('limit') : 20;
                $page       = $request->has('page') ? $request->input('page') : 1;
                $getdtlcat  = $getdtlcat->paginate($limit,['*'],'page',$page);
                $meta           = [
                    'page'      => (int) $getdtlcat->currentPage(),
                    'perPage'   => (int) $getdtlcat->perPage(),
                    'total'     => (int) $getdtlcat->total(),
                    'totalPage' => (int) $getdtlcat->lastPage()
                ];
                $getdtlcat   = $getdtlcat->toArray()['data'];
            }
    
            $response   = [
                'status'        => (bool) $status,
                'data'          => (isset($getdtlcat) ? $getdtlcat : null),
                'meta'          => (isset($meta) ? $meta : null),
                'error'         => (isset($error) ? $error : null)
            ];
            return response()->json($response);

        } catch (\Exception $e) {
            return response()->json(['message' => 'get child category failed!', 'error_report' => $e], 409);
        }


    }

    public function udpatedtlcategory(Request $request, $id)
    {
        $this->validate($request, [
            'dtlcat_name'           => 'required|string',
            'dtlcat_code'           => 'required|string',
            'dtlcat_companiesgroup' => 'required|string',
            'parentcode'            => 'required|string'
        ]);

        $datadtlcategory    = ItemCategoryModel::find($id);

        if ($datadtlcategory != null) {
            $params             = $request->all();
            $cekdtlcategory     = ItemCategoryModel::where(function ($query) use($request) {
                                        $query->where('dtlcat_name', $request->input('dtlcat_name'))
                                        ->orWhere('dtlcat_code', $request->input('dtlcat_code'));
                                    })->where('id','!=',$id);

            $cekdtlcategory     = $cekdtlcategory->first();

            if ($cekdtlcategory) {
                return response()->json(['message' => "Child category's Name or Code already exist!"], 409);
            } else {
                $datadtlcategory->fill($params);
                $datadtlcategory->save();
                return response()->json(['status' => (bool) true ,'message' => 'Your data has been update'], 200);
            }   
        }
    }

    public function deletedtlcategory(Request $request, $id)
    {
        $dtlcat       = ItemCategoryModel::find($id);
        if ($dtlcat) {
            $dtlcat->delete();
            return response()->json(['status' => (bool) true], 200);
        } else {
            return response()->json(['status' => (bool) false], 409);
        }
    }
}