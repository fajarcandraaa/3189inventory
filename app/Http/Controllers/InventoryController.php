<?php
namespace App\Http\Controllers;

use App\InventoryModel;
use App\LogInventoryModel;
use App\LogInvoiceModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Helper;
use Illuminate\Support\Str;
use Illuminate\Http\File as File;
class InventoryController extends Controller
{
    public function __construct()
    {
        $this->helper = new Helper;
    }
    // public function reciveitem(Request $request)
    // {
    //     if ($request->input('item_status') !== 'Repeat') {
    //         $this->validate($request, [
    //             'brand'             => 'required|string',
    //             'kodebarang2'       => 'required|string',
    //             'invoice_no'        => 'required|string',
    //             'item_category'     => 'required|string',
    //             'item_color'        => 'required|string',
    //             'title'             => 'string',
    //             'item_size'         => 'required|string',
    //             'hpp'               => 'string',
    //             'qty'               => 'required|integer',
    //             'price'             => 'required|string',
    //             'item_status'       => 'string',
    //             'item_imagesrc'     => 'string',
    //         ]);
    

    //         try {
    //             $loginventory               = new LogInventoryModel;
    //             $inventory                  = new InventoryModel;
    //             $invoice                    = new LogInvoiceModel;

    //             $lastdatafromlog            = $loginventory->where('kodebarang1','like','%'.$request->auth->companiesgroup_user.'%')
    //                                             ->orderBy('id','asc')
    //                                             ->first();
    //             $number                     = empty($lastdatafromlog['id']) ? 1 : substr($lastdatafromlog['kodebarang1'],-5)+1;
    //             $request['kodebarang1']     = $request->input('invoice_no') == $lastdatafromlog['invoice_no'] ? $lastdatafromlog['kodebarang1'] : $this->helper->generateinventorycode($request->auth->companiesgroup_user,date('n')-1,$number);
    //             $request['kodebarcode']     = $request->input('invoice_no') == $lastdatafromlog['invoice_no'] ? $lastdatafromlog['kodebarang1'].$request->input('item_size') : $this->helper->generateinventorycode($request->auth->companiesgroup_user,date('n')-1,$number,$request->input('item_size'));
    //             $request['totalasset']      = $request->input('qty')*$request->input('price');

    //             $cekloginventory            = $loginventory->where('kodebarcode',$request['kodebarcode'])
    //                                             ->where('invoice_no',$request->input('invoice_no'))
    //                                             ->first();
    //             $cekinvoicelog              = $invoice->where('invoice_no',$request->input('invoice_no'))->first();
    //             if ($cekloginventory) {
    //                 return response()->json(['message' => 'This item already exist!'], 409);
    //             } else {
    //                 $inventory  = $inventory->create([
    //                     'brand' => $request->input('brand'),
    //                     'kodebarcode'   => $request['kodebarcode'],
    //                     'item_category' => $request->input('item_category'),
    //                     'item_color'    => $request->input('item_color'),
    //                     'qty'           => $request->input('qty'),
    //                     'hpp'           => $request->input('hpp'),
    //                     'price'         => $request->input('price'),
    //                     'totalasset'    => $request['totalasset'],
    //                     'item_imagesrc' => $request->input('item_imagesrc')
    //                 ]);
    //                 $loginventory   = $loginventory->create([
    //                     'kodebarang1'   => $request['kodebarang1'],
    //                     'kodebarang2'   => $request->input('kodebarang2'),
    //                     'kodebarcode'   => $request['kodebarcode'],
    //                     'invoice_no'    => $request->input('invoice_no'),
    //                     'title'         => $request->input('title'),
    //                     'item_size'     => $request->input('item_size'),
    //                     'qty'           => $request->input('qty'),
    //                     'price'         => $request->input('price'),
    //                     'stock_akhir'   => $request->input('qty'),
    //                     'item_status'   => $request->input('item_status'),
    //                     'total'         => $request['totalasset']
    //                 ]);
                    
    //                 if ($cekinvoicelog) {
    //                     LogInvoiceModel::where('invoice_no',$request->input('invoice_no'))->update([
    //                                         'qty'           => $invoice['qty']+$request->input('qty'),
    //                                         'totalasset'    => $cekinvoicelog['totalasset'] + $request['totalasset']
    //                                     ]);
    //                 } else {
    //                     LogInvoiceModel::create([
    //                         'invoice_no' => $request->input('invoice_no'),
    //                         'qty' => $request->input('qty'),
    //                         'totalasset' => $request['totalasset'],
    //                         'status_invoice'    => $request->input('item_status')
    //                     ]);
    //                 }
    //                 return response()->json(['message' => 'New item was created'], 201);
    //             }
    //         } catch (\Exception $e) {
    //             return response()->json(['message' => 'inventory Registration Failed!', 'error_report' => $e], 409);
    //         }
    //     } else {
    //         $this->validate($request, [
    //             'brand'             => 'required|string',
    //             'kodebarcode'       => 'required|string',
    //             'kodebarang2'       => 'required|string',
    //             'invoice_no'        => 'required|string',
    //             'item_category'     => 'required|string',
    //             'item_color'        => 'required|string',
    //             'title'             => 'string',
    //             'item_size'         => 'required|string',
    //             'hpp'               => 'string',
    //             'qty'               => 'required|integer',
    //             'price'             => 'required|string',
    //             'item_status'       => 'string',
    //             'item_imagesrc'     => 'string',
    //         ]);

    //         try {
    //             $loginventory               = new LogInventoryModel;
    //             $inventory                  = new InventoryModel;
    //             $invoice                    = new LogInvoiceModel;

    //             $cekinventory               = $inventory->where('kodebarcode',$request->input('kodebarcode'))->first();
    //             $cekloginventory            = $loginventory->where('kodebarcode',$request->input('kodebarcode'))->first();
    //             $total                      = $request->input('qty')*$request->input('price');
    //             $cekinvoicelog              = $invoice->where('invoice_no',$request->input('invoice_no'))->first();

    //             LogInventoryModel::create([
    //                 'kodebarang1'   => $cekloginventory['kodebarang1'],
    //                 'kodebarang2'   => $request->input('kodebarang2'),
    //                 'kodebarcode'   => $request->input('kodebarcode'),
    //                 'invoice_no'    => $request->input('invoice_no'),
    //                 'title'         => $request->input('title'),
    //                 'item_size'     => $request->input('item_size'),
    //                 'qty'           => $request->input('qty'),
    //                 'price'         => $request->input('price'),
    //                 'stock_akhir'   => $cekinventory['qty']+$request->input('qty'),
    //                 'item_status'   => $request->input('item_status'),
    //                 'total'         => $total
    //             ]);

    //             InventoryModel::where('kodebarcode',$request->input('kodebarcode'))->update([
    //                 'qty' => $cekinventory['qty']+$request->input('qty'),
    //                 'totalasset' => $cekinventory['totalasset']+$total
    //             ]);

    //             if ($cekinvoicelog) {
    //                 LogInvoiceModel::where('invoice_no',$request->input('invoice_no'))->update([
    //                                     'qty'           => $cekinvoicelog['qty']+$request->input('qty'),
    //                                     'totalasset'    => $cekinvoicelog['totalasset'] + $total
    //                                 ]);
    //             } else {
    //                 LogInvoiceModel::create([
    //                     'invoice_no' => $request->input('invoice_no'),
    //                     'qty' => $request->input('qty'),
    //                     'totalasset' => $total,
    //                     'status_invoice'    => $request->input('item_status')
    //                 ]);
    //             }
    //             return response()->json(['message' => 'Repeat item was added'], 201);
    //         } catch (\Exception $e) {
    //             return response()->json(['message' => 'inventory Registration Failed!', 'error_report' => $e], 409);
    //         }
    //     }
    // }

    public function generatekodebarang(Request $request)
    {
        $grupuser           = $request->auth->companiesgroup_user;
        $month              = date('n')-1;
        $lastdatafromlog    = LogInventoryModel::where('kodebarang1','like','%'.$request->auth->companiesgroup_user.'%')
                                ->orderBy('id','asc')
                                ->first();
        $number             = empty($lastdatafromlog['id']) ? 1 : substr($lastdatafromlog['kodebarang1'],-5)+1;
        $bulaninalphabet = [
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L'
        ];
        $code = $grupuser.'-'.substr(date("Y"),-2).$bulaninalphabet[$month].sprintf('%05d',$number);

        return $code;
    }
    
    public function getkodebarcodebybrand(Request $request)
    {
        try {
            $inventory  = InventoryModel::select('kodebarcode')->where('brand', $request->auth->companiesgroup_user)->get();
            
            if (count($inventory) > 0) {
                return response()->json(['messege' => 'success', 'data' => $inventory],200);
            } else {
                return response()->json(['message' => "Please checck your brand's name"], 409);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'Please checck your brand', 'error_report' => $e], 409);
        }
    }

    public function cekinventory(Request $request)
    {
        $status         = false;
        $error          = "data not found";
        $getinventory   = InventoryModel::with(['brand'])->where('brand',$request->auth->companiesgroup_user);
        
        if ($request->has('item_category')) {
            $getinventory = $getinventory->where('item_category', $request->input('item_category'));
        }


        if ($getinventory->get()) {
            $status         = true;
            $error          = null;
            $limit          = $request->has('limit') ? $request->input('limit') : 20;
            $page           = $request->has('page') ? $request->input('page') : 1;
            $getinventory   = $getinventory->paginate($limit,['*'],'page',$page);
            $meta           = [
                'page'      => (int) $getinventory->currentPage(),
                'perPage'   => (int) $getinventory->perPage(),
                'total'     => (int) $getinventory->total(),
                'totalPage' => (int) $getinventory->lastPage()
            ];
            $getinventory   = $getinventory->toArray()['data'];
        }

        $response = [
            "status"    => (bool) $status,
            "data"      => (isset($getinventory) ?$getinventory : null),
            "meta"      => (isset($meta) ? $meta : null),
            "error"     => (isset($error) ? $error : null)
        ];
        return response()->json($response);
    }

    public function uploadimagecatalog(Request $request)
    {
        $this->validate($request,[
            'avatar'    => 'required|image',
            'brand'     => 'required|string'
        ]);
        $imageName  = str::random(34).'.jpg';
        $path       = storage_path($request->input('brand'));
        if (file_exists($path)) {
            $request->file('avatar')->move($path,$imageName);
        } else {
            mkdir($path,0700);
            $request->file('avatar')->move($path,$imageName);
        }

        $response = [
            'messege' => 'success',
            'pathurl' => $path.'/'.$imageName
        ];

        return $response;
    }

    public function changeimage(Request $request)
    {
        $this->validate($request,[
            'avatar'        => 'required|image',
            'brand'         => 'required|string',
            'kodebarcode'   => 'required|string'
        ]);
        
        $path               = $this->uploadimagecatalgo($request);
        InventoryModel::where('kodebarcode',$request->input('kodebarcode'))->update(['item_imagesrc' => $path['pathurl']]);

        return response()->json(['messege' => "Your item's image was change"],200);
    }

    public function batchinsert(Request $request)
    {
        $grupuser       = $request->auth->companiesgroup_user;
        $loginventory   = new LogInventoryModel;
        $inventory      = new InventoryModel;
        $invoice        = new LogInvoiceModel;
        $cekinvoicelog  = $invoice->where('invoice_no',$request->datainput[0]['invoice_no'])->first();
        $invoqty        = 0;
        $invototal      = 0;
        $invoqtyrepeat  = 0;
        $invototalrepeat= 0;
        $isRepeat       = false;
        foreach ($request->datainput as $req) {

            if (strpos($req['item_status'], "NEW") !== false) {
                $datanew[] = $req;
                $req['kodebarcode'] = $req['kodebarang1'].$req['item_size'];
                $req['totalasset']  = $req['price']*$req['qty'];
                
                $dataforinvnetory[] = [
                    "brand"         => $req['brand'],
                    "kodebarcode"   => $req['kodebarcode'],
                    "item_category" => $req['item_category'],
                    "item_color"    => $req['item_color'],
                    "qty"           => $req['qty'],
                    "hpp"           => $req['hpp'],
                    "price"         => $req['price'],
                    "totalasset"    => $req['totalasset'],
                    "item_imagesrc" => $req['item_imagesrc'],
                    'created_at'    => date("Y-m-d h:i:s")
                ];

                $dataforlog[] = [
                    "kodebarang1"           => $req['kodebarang1'],
                    "kodebarang2"           => $req['kodebarang2'],
                    "kodebarcode"           => $req['kodebarcode'],
                    "invoice_no"            => $req['invoice_no'],
                    "title"                 => $req['title'],
                    "item_size"             => $req['item_size'],
                    "qty"                   => $req['qty'],
                    "price"                 => $req['price'],
                    "stock_akhir"           => $req['qty'],
                    "item_status"           => $req['item_status'],
                    "total"                 => $req['totalasset'],
                    'created_at'            => date("Y-m-d h:i:s")
                ];

                $invoqty    += $req['qty'];
                $invototal  += $req['totalasset'];
            } 

            if (strpos($req['item_status'], "REPEAT") !== false) {
                $datarepeat[]       = $req;
                $isRepeat           = true;
                $req['totalasset']  = $req['price']*$req['qty'];
                $cekstock           = InventoryModel::where('kodebarcode',$req['kodebarcode'])->first();
                $dataforlogrepeat[] = [
                    "kodebarang1"           => $req['kodebarang1'],
                    "kodebarang2"           => $req['kodebarang2'],
                    "kodebarcode"           => $req['kodebarcode'],
                    "invoice_no"            => $req['invoice_no'],
                    "title"                 => $req['title'],
                    "item_size"             => $req['item_size'],
                    "qty"                   => $req['qty'],
                    "price"                 => $req['price'],
                    "stock_akhir"           => $cekstock->qty + $req['qty'],
                    "item_status"           => $req['item_status'],
                    "total"                 => $req['totalasset'],
                    'created_at'            => date("Y-m-d h:i:s")
                ];

                $invoqtyrepeat    += $req['qty'];
                $invototalrepeat  += $req['totalasset'];
            }
        }

        if ($isRepeat) {
            //input new item
            if (!empty($dataforinvnetory)) {
                $inventory->insert($dataforinvnetory);
                $loginventory->insert($dataforlog);
            }

            //input repeat item
            $loginventory->insert($dataforlogrepeat);
            foreach ($dataforlogrepeat as $repeat) {
                $cekinventory   = $inventory->where('kodebarcode',$repeat['kodebarcode'])->first();
                $total          = $repeat['qty']*$repeat['price'];
                $adasdasdsa[]   = $total;
                $inventory->where('kodebarcode',$repeat['kodebarcode'])->update([
                    'qty' => $cekinventory['qty']+$repeat['qty'],
                    'totalasset' => $cekinventory['totalasset']+$total
                ]);
            }
            
            LogInvoiceModel::create([
                'invoice_no'        => $request->datainput[0]['invoice_no'],
                'qty'               => !empty($dataforinvnetory) ? $invoqty+$invoqtyrepeat : $invoqtyrepeat,
                'totalasset'        => !empty($dataforinvnetory) ? $invototal+$invototalrepeat : $invototalrepeat,
                'status_invoice'    => !empty($dataforinvnetory) ? "New & Repeat" : "Repeat"
            ]);
            return response()->json(['message' => !empty($dataforinvnetory) ? 'New item was created and repeat item was update' : 'Repeat item was added'], 201);
        } else {
            $inventory->insert($dataforinvnetory);
            $loginventory->insert($dataforlog);
            LogInvoiceModel::create([
            'invoice_no'        => $request->datainput[0]['invoice_no'],
            'qty'               => $invoqty,
            'totalasset'        => $invototal,
            'status_invoice'    => $request->datainput[0]['item_status']
            ]);
            return response()->json(['message' => 'New item was created'], 201);
        }
    }
}