<?php
namespace App\Http\Controllers;
use App\SizeModel;
use App\ItemCategoryModel;
use App\CategoryModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class SizeController extends Controller
{
    public function addnewsize(Request $request)
    {
        $this->validate($request, [
            'size_name'     => 'required|string',
            'category_code' => 'required|string'
        ]);

        try {
            $size       = new SizeModel;
            $params     = $request->all();
            $size->fill($params);

            $ceksize    = $size->where('size_name',$request->input('size_name'))
                            ->where('category_code',$request->input('category_code'))
                            ->first();

            if ($ceksize) {
                return response()->json(['message' => 'This size already exist!'], 409);
            } else {
                $size->save();
                return response()->json(['size' => $size, 'message' => 'CREATED'], 201);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'Size Registration Failed!', 'error_report' => $e], 409);
        }
    }

    public function getallsize(Request $request)
    {
        $status = false;
        $error  = "data not found";
        $getsize = new SizeModel;

        if ($request->has('group_item')) {
            $getsize    = $getsize->where('group_item',$request->input('group_item'))->get();

            if ($getsize) {
                $status = true;
                $error  = null;
            }
        } else {
            $status     = true;
            $error      = null;

            $limit      = $request->has('limit') ? $request->input('limit') : 20;
            $page       = $request->has('page') ? $request->input('page') : 1;
            $getsize    = $getsize->paginate($limit,['*'],'page',$page);
            $meta       = [
                'page'      => (int) $getsize->currentPage(),
                'perPage'   => (int) $getsize->perPage(),
                'total'     => (int) $getsize->total(),
                'totalPage' => (int) $getsize->lastPage()
            ];
            $getsize   = $getsize->toArray()['data'];
        }

        $response = [
            "status"    => (bool) $status,
            "data"      => (isset($getsize) ? $getsize : null),
            "meta"      => (isset($meta) ? $meta : null),
            "error"     => (isset($error) ? $error : null)
        ];
        return response()->json($response);
    }

    public function getsizebycategory(Request $request)
    {
        $gettparent = ItemCategoryModel::where('dtlcat_code',$request->input('item_category'))->first();
        $groupItem  = CategoryModel::where('category_code', $gettparent->parentcode)->first();
        
        $size = SizeModel::where('group_item',$groupItem->group_item)->get('size_name');

        return $size;
        
    }
    public function deletesize(Request $request, $id)
    {
        $size   = SizeModel::find($id);
        if ($size) {
            $size->delete();
            return response()->json(['status' => (bool) true], 200);
        } else {
            return response()->json(['status' => (bool) false], 409);
        }
    }
}