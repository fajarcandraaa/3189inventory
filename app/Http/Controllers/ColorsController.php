<?php
namespace App\Http\Controllers;
use App\ColorsModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class ColorsController extends Controller
{
    public function addnewcolor(Request $request)
    {
        $this->validate($request, [
            'colors_name'           => 'required|string',
            'colors_code'           => 'required|string'
        ]);

        try {
            $colors    = new ColorsModel;
            $params         = $request->all();
            $colors->fill($params);

            $cekcolors     = $colors->where('colors_name', $request->input('colors_name'))
                                    ->where('colors_code', $request->input('colors_code'))
                                    ->first();

            if ($cekcolors) {
                return response()->json(['message' => 'This color already exist!'], 409);
            } else {
                $colors->save();
                return response()->json(['dtl_category' => $colors, 'message' => 'CREATED'], 201);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'Your color Registration Failed!', 'error_report' => $e], 409);
        }
    }

    public function getallcolor(Request $request)
    {
        $status         = false;
        $error          = "data not found";
        $getcolors      = new ColorsModel;

        try {
            if ($getcolors->get()) {
                $status     = true;
                $error      = null;
                $limit      = $request->has('limit') ? $request->input('limit') : 20;
                $page       = $request->has('page') ? $request->input('page') : 1;
                $getcolors  = $getcolors->paginate($limit,['*'],'page',$page);
                $meta           = [
                    'page'      => (int) $getcolors->currentPage(),
                    'perPage'   => (int) $getcolors->perPage(),
                    'total'     => (int) $getcolors->total(),
                    'totalPage' => (int) $getcolors->lastPage()
                ];
                $getcolors   = $getcolors->toArray()['data'];
            }
    
            $response   = [
                'status'        => (bool) $status,
                'data'          => (isset($getcolors) ? $getcolors : null),
                'meta'          => (isset($meta) ? $meta : null),
                'error'         => (isset($error) ? $error : null)
            ];
            return response()->json($response);

        } catch (\Exception $e) {
            return response()->json(['message' => 'get child category failed!', 'error_report' => $e], 409);
        }


    }

    public function deletecolor(Request $request, $id)
    {
        $color       = ColorsModel::find($id);
        if ($color) {
            $color->delete();
            return response()->json(['status' => (bool) true], 200);
        } else {
            return response()->json(['status' => (bool) false], 409);
        }
    }
}