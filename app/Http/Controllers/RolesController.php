<?php
namespace App\Http\Controllers;

use App\RolesModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class RolesController extends Controller
{
    public function addnewroles(Request $request)
    {
        $this->validate($request,[
            'role_name'             => 'required|string',
            'role_code'             => 'required|string',
            'role_companiesgroup'   => 'required|string'
        ]);

        try {
            $roles  = new RolesModel;
            $params = $request->all();
            $roles->fill($params);

            $cekroles   = $roles->where('role_code', $request->input('role_code'))
                            ->where('role_companiesgroup',$request->input('role_companiesgroup'))
                            ->first();

            if ($cekroles) {
                return response()->json(['message' => 'This roles already exist!'], 409);
            } else {
                $roles->save();
                return response()->json(['roles' => $roles, 'message' => 'CREATED'], 201);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'Roles Registration Failed!', 'error_report' => $e], 409);
        }
    }

    public function getallroles(Request $request)
    {
        $status         = false;
        $error          = "data not found";
        $getrolesuser   = RolesModel::with(['role_companiesgroup']);

        if ($getrolesuser->get()) {
            $status         = true;
            $error          = null;
            $limit          = $request->has('limit') ? $request->input('limit') : 20;
            $page           = $request->has('page') ? $request->input('page') : 1;
            $getrolesuser   = $getrolesuser->paginate($limit,['*'],'page',$page);
            $meta           = [
                'page'      => (int) $getrolesuser->currentPage(),
                'perPage'   => (int) $getrolesuser->perPage(),
                'total'     => (int) $getrolesuser->total(),
                'totalPage' => (int) $getrolesuser->lastPage()
            ];
            $getrolesuser   = $getrolesuser->toArray()['data'];
        }

        $response   = [
            'status'        => (bool) $status,
            'data'          => (isset($getrolesuser) ? $getrolesuser : null),
            'meta'          => (isset($meta) ? $meta : null),
            'error'         => (isset($error) ? $error : null)
        ];
        return response()->json($response);
    }

    public function updateroles(Request $request, $id)
    {
        $this->validate($request,[
            'role_name'             => 'required|string',
            'role_code'             => 'required|string'
        ]);

        $dataroles  = RolesModel::find($id);
        
        if ($dataroles != null) {
            $params     = $request->all();
            $cekroles   = RolesModel::where(function ($query) use($request) {
                                $query->where('role_name', $request->input('role_name'))
                                ->orWhere('role_code', $request->input('role_code'));
                            })->where('id','!=',$id);

            $cekroles   = $cekroles->first();
            
            if ($cekroles) {
                return response()->json(['message' => "Role's Name or Code already exist!"], 409);
            } else {
                $dataroles->fill($params);
                $dataroles->save();
                return response()->json(['status' => (bool) true ,'message' => 'Your data has been update'], 200);
            }    
        }
    }

    public function deleteroles(Request $request, $id)
    {
        $role       = RolesModel::find($id);
        if ($role) {
            if ($role['role_code'] == "sadm") {
                return response()->json(['status' => (bool) false, 'messege' => "Can't delete superadmin"], 409);
            } else {
                $role->delete();
                return response()->json(['status' => (bool) true], 200);
            }
        } else {
            return response()->json(['status' => (bool) false], 409);
        }
    }
}