<?php
namespace App\Http\Controllers;
use App\StoreoutModel;
use App\InventoryModel;
use App\LogInventoryModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\Helper;

class StoreoutController extends Controller
{

    public function __construct()
    {
        $this->helper = new Helper;
    }

    public function sotrout(Request $request)
    {
        date_default_timezone_set("Asia/Bangkok");
        $cashier        = $request->auth->id;
        $storeout       = new StoreoutModel;
        $messege        = "Ready Stock";
        $notrans        = $this->helper->generatenotrans($request->auth->companiesgroup_user,date('n')-1);
        $allprice       = 0;
        foreach ($request->storeout as $req) {
            $cekstock       = InventoryModel::where('kodebarcode',$req['kodebarcode'])->first();
            $ceklastlog     = LogInventoryModel:: where('kodebarcode',$req['kodebarcode'])
                            ->where('item_status','New')
                            ->first();
            $datastoreout[] = [
                "notrans"       => $notrans,
                "brand"         => $req['brand'],
                "kodebarcode"   => $req['kodebarcode'],
                "qty"           => $req['qty'],
                "totalprice"    => $req['qty']*$cekstock->price,
                'cashier'       => $cashier,
                'created_at'    => date("Y-m-d h:i:s")
            ];
            
            $datalog[]  = [
                'kodebarang1'   => $ceklastlog->kodebarang1,
                'kodebarang2'   => $ceklastlog->kodebarang2,
                'kodebarcode'   => $req['kodebarcode'],
                'invoice_no'    => 'none',
                'title'         => $ceklastlog->title,
                'item_size'     => $ceklastlog->item_size,
                'qty'           => '-'.$req['qty'],
                'price'         => $cekstock->price,
                'stock_akhir'   => $cekstock->qty - $req['qty'],
                'item_status'   => 'Sold',
                'total'         => $cekstock->price*$req['qty'],
                'created_at'    => date("Y-m-d h:i:s")
            ];

            $detailitem[]   = [
                "itemName"  => $req['brand'],
                "itemCode"  => $req['kodebarcode'],
                "itemQty"   => $req['qty'],
                "itemPrice" => $req['qty']*$cekstock->price
            ];

            InventoryModel::where('kodebarcode',$req['kodebarcode'])->update([
                "qty"           => $cekstock->qty - $req['qty'],
                "totalasset"    => $cekstock->totalasset - ($req['qty']*$cekstock->price)
            ]);
        }
        // return $datalog;
        StoreoutModel::insert($datastoreout);
        LogInventoryModel::insert($datalog);

        $response = [
            'message'   => 'Success',
            'data'      =>[
                'No_Trans'  => $notrans, 
                'Date'      => date("l jS \of F Y h:i:s A"), 
                'Item'      => $detailitem,
                ]
        ];

        foreach ($response['data']['Item'] as $item) {
            $allprice += $item['itemPrice'];
        }

        $response['data']['subTotal'] = $allprice;

        return response()->json($response);
    }

    public function getstock(Request $request)
    {
        $stock  = InventoryModel::where('kodebarcode',$request->input('kodebarcode'))->first();
        if ($stock) {
            if ($stock <= $request->input('qty')) {
                return response()->json(['message' => "Our Stock", 'stock' => $stock->qty],409);
            } else {
                return response()->json(['message' => "The number of items you are going to buy exceeds this item's stock"],409);
            }
        } else {
            return response()->json(['message' => "We don't haave this item"],409);
        }
    }
}