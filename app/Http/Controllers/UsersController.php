<?php
namespace App\Http\Controllers;

use App\UsersModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class UsersController extends Controller
{
    public function registernewuser(Request $request)
    {
        $this->validate($request, [
            'name'                  => 'required|string',
            'username'              => 'required|string',
            'address'               => 'required|string',
            'phone'                 => 'required|string',
            'email'                 => 'required|email',
            'role_user'             => 'required|string',
            'companiesgroup_user'   => 'required|string',
        ]);

        try {
            $haser                  = app()->make('hash');
            $users                  = new UsersModel;
            $password               = "123qweasd";
            $request['password']    = $haser->make($password);
            $params                 = $request->all();
            $users->fill($params);

            $cekusers = $users->where('username', $request->input('username'))
                        ->where('email', $request->input('email'))
                        ->first();
            if ($cekusers) {
                return response()->json(['message' => 'This users already exist!'], 409);
            } else {
                $users->save();
                return response()->json(['users' => $users, 'message' => 'CREATED'], 201);
            }

        } catch (\Exception $e) {
            return response()->json(['message' => 'Users Registration Failed!', 'error_report' => $e], 409);
        }
    }

    public function getallusers(Request $request, $id = null)
    {
        $status     = false;
        $error      = "data not found";
        $getusers   = UsersModel::with(['role_user','companiesgroup_user']);

        if ($id) {
            $getusers = $getusers->where('id',$id)->first();

            if($getusers){
                $status = true;
                $error  = null;
            }
        } else {
            $getusers   = $getusers;
            $status     = true;
            $error      = null;

            $limit      = $request->has('limit') ? $request->input('limit') : 20;
            $page       = $request->has('page') ? $request->input('page') : 1;
            $getusers   = $getusers->paginate($limit,['*'],'page',$page);
            $meta       = [
                'page'      => (int) $getusers->currentPage(),
                'perPage'   => (int) $getusers->perPage(),
                'total'     => (int) $getusers->total(),
                'totalPage' => (int) $getusers->lastPage()
            ];
            $getusers   = $getusers->toArray()['data'];
        }

        $response = [
            "status"    => (bool) $status,
            "data"      => (isset($getusers) ?$getusers : null),
            "meta"      => (isset($meta) ? $meta : null),
            "error"     => (isset($error) ? $error : null)
        ];
        return response()->json($response);
    }

    public function updateusers(Request $request, $id)
    {
        $this->validate($request, [
            'name'          => 'required|string',
            'username'      => 'string',
            'email'         => 'required|email'
        ]);

        $datauser   = UsersModel::find($id);

        if ($datauser != null) {
            $params = $request->all();
            $cekuser = UsersModel::where(function ($query) use($request) {
                                $query->where('username', $request->input('username'))
                                ->orWhere('email', $request->input('email'));
                            })->where('id','!=',$id);

            $cekuser = $cekuser->first();
            if ($cekuser) {
                return response()->json(['message' => 'Username or email already exist!'], 409);
            } else {
                $datauser->fill($params);
                $datauser->save();
                return response()->json(['status' => (bool) true ,'message' => 'Your data has been update'], 200);
            }     
        } else {
            return response()->json(['status' => (bool) false, 'message' => 'Something wrong when update data'], 409);
        }
    }

    public function deleteusers(Request $request, $id)
    {
        $user   = UsersModel::find($id);
        if ($user) {
            $user->delete();
            return response()->json(['status' => (bool) true], 200);
        } else {
            return response()->json(['status' => (bool) false], 409);
        }
    }
}