<?php
namespace App\Http\Controllers;

use App\CompaniesModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CompaniesController extends Controller
{
    public function addnewcompany(Request $request)
    {
        $this->validate($request,[
            'companies_name'                => 'required|string',
            'companies_address'             => 'required|string',
            'companies_code'                => 'required|string',
            'companies_phone'               => 'string'
        ]);
            
        
        try {
            $companies  = new CompaniesModel;
            $params     = $request->all();
            $companies->fill($params);
            
            
            $cekroles   = CompaniesModel::where('companies_code', $request->input('companies_code'))
                            ->orWhere('companies_code',$request->input('companies_code'))
                            ->first();
                            
            if ($cekroles) {
                return response()->json(['message' => 'This companies already exist!'], 409);
            } else {
                $companies->save();
                // File::makeDirectory()
                return response()->json(['companies' => $companies, 'message' => 'CREATED'], 201);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'Companies Registration Failed!', 'error_report' => $e], 409);
        }
    }

    public function getcompanies(Request $request, $id = null)
    {
        $status     = false;
        $error      = "data not found";
        $getcompanies   = new CompaniesModel;

        if ($id) {
            $getcompanies = $getcompanies->where('id',$id)->first();

            if($getcompanies){
                $status = true;
                $error  = null;
            }
        } else {
            $getcompanies   = $getcompanies;
            $status     = true;
            $error      = null;

            $limit      = $request->has('limit') ? $request->input('limit') : 20;
            $page       = $request->has('page') ? $request->input('page') : 1;
            $getcompanies   = $getcompanies->paginate($limit,['*'],'page',$page);
            $meta       = [
                'page'      => (int) $getcompanies->currentPage(),
                'perPage'   => (int) $getcompanies->perPage(),
                'total'     => (int) $getcompanies->total(),
                'totalPage' => (int) $getcompanies->lastPage()
            ];
            $getcompanies   = $getcompanies->toArray()['data'];
        }

        $response = [
            "status"    => (bool) $status,
            "data"      => (isset($getcompanies) ?$getcompanies : null),
            "meta"      => (isset($meta) ? $meta : null),
            "error"     => (isset($error) ? $error : null)
        ];
        return response()->json($response);
    }

    public function updatecompanies(Request $request, $id)
    {
        $this->validate($request,[
            'companies_name'                => 'required|string',
            'companies_code'                => 'required|string'
        ]);

        $datacompanies      = CompaniesModel::find($id);

        if ($datacompanies != null) {
            $params         = $request->all();
            $cekcompanies   = CompaniesModel::where(function ($query) use($request) {
                                    $query->where('companies_name', $request->input('companies_name'))
                                    ->orWhere('companies_code', $request->input('companies_code'));
                                })->where('id','!=',$id);
            $cekcompanies   = $cekcompanies->first();
            if ($cekcompanies) {
                return response()->json(['message' => "This companie's name or code already exist!"], 409);
            } else {
                $datacompanies->fill($params);
                $datacompanies->save();
                return response()->json(['status' => (bool) true ,'message' => "Your companie's data has been update"], 200);
            }
        } else {
            return response()->json(['status' => (bool) false, 'message' => 'Something wrong when update data'], 409);
        }
    }

    public function deletecompanies(Request $request, $id)
    {
        $companies  = CompaniesModel::find($id);
        if ($companies) {
            $companies->delete();
            return response()->json(['status' => (bool) true], 200);
        } else {
            return response()->json(['status' => (bool) false], 409);
        }
    }
}