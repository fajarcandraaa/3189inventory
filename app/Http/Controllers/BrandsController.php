<?php
namespace App\Http\Controllers;

use App\BrandsModel;
use App\CompaniesModel;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class BrandsController extends Controller
{
    public function addnewbrand(Request $request)
    {
        $this->validate($request, [
            'brand_name'        => 'required|string',
            'brand_initials'    => 'required|string'
        ]);
        $item = CompaniesModel::where('companies_code',$request->auth->companiesgroup_user)->first();

        try {
            $cekbrand       = BrandsModel::where('brand_name',$request->input('brand_name'))
                                ->orWhere('brand_initials',$request->input('brand_initials'))
                                ->first();
            if ($cekbrand) {
                return response()->json(['message' => 'This brand already exist'], 409);
            } else {
                $brand = BrandsModel::create([
                    'brand_name'    => $request->input('brand_name'),
                    'brand_initials'    => $request->input('brand_initials'),
                    'brand_address'     => $item->companies_address,
                    'brand_phone'       => $item->companies_phone,
                    'brand_companiesgroup'  => $request->auth->companiesgroup_user
                ]);
    
                return response()->json(['message' => 'Success', 'brand' => $brand], 200);
            }
        } catch (\Exception $e) {
            return response()->json(['message' => 'Failed to register new brand'], 409);
        }
    }

    public function getallbrands(Request $request)
    {
        $status     = false;
        $error      = "data not found";
        $getbrand      = BrandsModel::with(['brand_companiesgroup']);

        if ($getbrand->get()) {
            $status     = true;
            $error      = null;

            $limit      = $request->has('limit') ? $request->input('limit') : 20;
            $page       = $request->has('page') ? $request->input('page') : 1;
            $getbrand   = $getbrand->paginate($limit,['*'],'page',$page);
            $meta       = [
                'page'      => (int) $getbrand->currentPage(),
                'perPage'   => (int) $getbrand->perPage(),
                'total'     => (int) $getbrand->total(),
                'totalPage' => (int) $getbrand->lastPage()
            ];
            $getbrand   = $getbrand->toArray()['data'];
        }

        $response = [
            "status"    => (bool) $status,
            "data"      => (isset($getbrand) ?$getbrand : null),
            "meta"      => (isset($meta) ? $meta : null),
            "error"     => (isset($error) ? $error : null)
        ];
        return response()->json($response);

    }

    public function getallbrandsbycompanies(Request $request)
    {
        $status     = false;
        $error      = "data not found";
        $getbrand      = BrandsModel::with(['brand_companiesgroup']);

        if ($getbrand->get()) {
            $getbrand   = $getbrand->where('brand_companiesgroup',$request->auth->companiesgroup_user);
            $status     = true;
            $error      = null;

            $limit      = $request->has('limit') ? $request->input('limit') : 20;
            $page       = $request->has('page') ? $request->input('page') : 1;
            $getbrand   = $getbrand->paginate($limit,['*'],'page',$page);
            $meta       = [
                'page'      => (int) $getbrand->currentPage(),
                'perPage'   => (int) $getbrand->perPage(),
                'total'     => (int) $getbrand->total(),
                'totalPage' => (int) $getbrand->lastPage()
            ];
            $getbrand   = $getbrand->toArray()['data'];
        }

        $response = [
            "status"    => (bool) $status,
            "data"      => (isset($getbrand) ?$getbrand : null),
            "meta"      => (isset($meta) ? $meta : null),
            "error"     => (isset($error) ? $error : null)
        ];
        return response()->json($response);

    }

    public function updatebranddata(Request $request, $id)
    {
        $this->validate($request, [
            'brand_name'        => 'required|string',
            'brand_initials'    => 'required|string'
        ]);

        $databrand  = BrandsModel::find($id);

        if ($databrand !== null) {
            $params     = $request->all();
            $cekbrand   = BrandsModel::where(function ($query) use($request) {
                                $query->where('brand_name', $request->input('brand_name'))
                                ->orWhere('brand_initials', $request->input('brand_initials'));
                            })->where('id','!=',$id);
            $cekbrand   = $cekbrand->first();
            if ($cekbrand) {
                return response()->json(['message' => "Brand's name or initials already exist!"], 409);
            } else {
                $databrand->fill($params);
                $databrand->save();
                return response()->json(['status' => (bool) true ,'message' => 'Your data has been update'], 200);
            }     
        } else {
            return response()->json(['status' => (bool) false, 'message' => 'Something wrong when update data'], 409);
        }
    }

    public function deletebrand(Request $request, $id)
    {
        $brand  = BrandsModel::find($id);
        if ($brand) {
            $brand->delete();
            return response()->json(['status' => (bool) true], 200);
        } else {
            return response()->json(['status' => (bool) false], 409);
        }
    }
}