<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class StoreoutModel extends Model
{
    protected $table = 'storeout';

    protected $fillable = [
        'notrans',
        'brand',
        'kodebarcode',
        'qty',
        'totalprice',
        'cashier'
    ];
}