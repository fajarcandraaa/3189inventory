<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ItemCategoryModel extends Model
{
    use SoftDeletes;
    protected $table = 'mstr_dtlcategory';

    public function dtlcat_companiesgroup(){
        return $this->belongsTo('App\CompaniesModel', 'dtlcat_companiesgroup','companies_code');
    }

    public function parentcode(){
        return $this->belongsTo('App\CategoryModel', 'parentcode','category_code');
    }

    protected $fillable = [
        'dtlcat_name',
        'dtlcat_code',
        'dtlcat_description',
        'dtlcat_companiesgroup',
        'parentcode'
    ];

    protected $hidden = [
        
    ];
}