<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\ControllerMakeCommand::class,
        Commands\ModelMakeCommand::class,
        Commands\DistributeTaskCommand::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $envdates = date("H:i",strtotime(env('STARTASSIGN')));
        $envdatef = date("H:i",strtotime(env('ENDASSIGN')));

        $schedule->command('claim:distributeTask')
            ->weekdays()
            ->hourly()
            ->timezone('Asia/Jakarta')
            ->between($envdates, $envdatef);
    }

    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
