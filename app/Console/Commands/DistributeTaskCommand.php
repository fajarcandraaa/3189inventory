<?php 
namespace App\Console\Commands;
use Illuminate\Console\Command;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

class DistributeTaskCommand extends Command
{
     /**
    * The name and signature of the console command.
    *
    * @var string
    */
   protected $signature = 'claim:distributeTask';
   /**
    * The console command description.
    *
    * @var string
    */
   protected $description = 'Command distribute claim data to analyst';
   /**
    * Create a new command instance.
    *
    * @return void
    */
   public function __construct()
   {
       parent::__construct();
   }
   /**
    * Execute the console command.
    *
    * @return mixed
    */
   public function handle()
   {
        //call assign distribute claim here
        log::debug(time());
   }
}