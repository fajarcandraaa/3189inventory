<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class InventoryModel extends Model
{
    use SoftDeletes;
    protected $table = 'inventory';

    public function brand(){
        return $this->belongsTo('App\CompaniesModel', 'brand','companies_code');
    }
    protected $fillable = [
        'brand',
        'kodebarcode',
        'item_category',
        'item_color',
        'qty',
        'hpp',
        'price',
        'totalasset',
        'item_imagesrc'
    ];
}