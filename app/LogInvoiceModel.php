<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class LogInvoiceModel extends Model
{
    protected $table = 'invoice_transaction';

    protected $fillable = [
        'invoice_no',
        'qty',
        'totalasset',
        'status_invoice'
    ];
}