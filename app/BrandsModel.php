<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class BrandsModel extends Model
{
    use SoftDeletes;
    protected $table = 'mstr_brands';

    public function brand_companiesgroup(){
        return $this->belongsTo('App\CompaniesModel', 'brand_companiesgroup','companies_code');
    }

    protected $fillable = [
        'brand_name',
        'brand_initials',
        'brand_address',
        'brand_phone',
        'brand_companiesgroup'
    ];
}