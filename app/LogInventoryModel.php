<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class LogInventoryModel extends Model
{
    protected $table = 'log_inventory';

    protected $fillable = [
        'kodebarang1',
        'kodebarang2',
        'kodebarcode',
        'invoice_no',
        'title',
        'item_size',
        'qty',
        'price',
        'stock_akhir',
        'item_status',
        'total'
    ];
}