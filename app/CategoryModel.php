<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class CategoryModel extends Model
{
    use SoftDeletes;
    protected $table = 'mstr_categoryitem';

    public function category_code(){
        return $this->hasMany('App\ItemCategoryModel', 'parentcode', 'category_code');
    }

    protected $fillable = [
        'category_name',
        'category_code',
        'category_description',
        'group_item'
    ];

    protected $hidden = [
        
    ];
}