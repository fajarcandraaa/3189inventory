<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class SizeModel extends Model
{
    protected $table = 'mstr_size';

    public function category_code()
    {
        return $this->belongsTo('App\CategoryModel','category_code','category_code');
    }

    protected $fillable = [
        'size_name',
        'category_code',
        'size_description',
        'size_companiesgroup',
        'group_item'
    ];

    protected $hidden = [
        
    ];
}