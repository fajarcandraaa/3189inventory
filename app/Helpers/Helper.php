<?php
namespace App\Helpers;

use App\InventoryModel;
use App\StoreoutModel;
use Illuminate\Http\Request;
class Helper
{
    public function generatenotrans($companies,$month)
    {
        $bulaninalphabet = [
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L'
        ];
        $format1 = $companies.'-'.$bulaninalphabet[$month].substr(date("Y"),-2);
        $ceknumber  = StoreoutModel::where('notrans','like','%'.$format1.'%')->first();
        $number     = empty($ceknumber['id']) ? 1 : substr($ceknumber['notrans'],-10)+1;
        $format2 = sprintf('%010d',$number);
        
        
        $code = $format1.$format2;

        return $code;
    }
}