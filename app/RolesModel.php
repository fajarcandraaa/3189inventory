<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class RolesModel extends Model
{
    use SoftDeletes;
    protected $table = 'mstr_rolepermissions';

    public function role_companiesgroup(){
        return $this->belongsTo('App\CompaniesModel', 'role_companiesgroup','companies_code');
    }

    protected $fillable = [
        'role_name',
        'role_code',
        'role_companiesgroup'
    ];

    protected $hidden = [
        
    ];
}