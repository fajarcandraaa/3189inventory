<?php
namespace App;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class UsersModel extends Model  implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable, SoftDeletes;
    protected $table = 'mstr_users';

    public function role_user(){
        return $this->belongsTo('App\RolesModel', 'role_user','role_code');
    }

    public function companiesgroup_user(){
        return $this->belongsTo('App\CompaniesModel', 'companiesgroup_user','companies_code');
    }

    protected $fillable = [
        'name',
        'address',
        'phone',
        'dateOfBirth',
        'email',
        'role_user',
        'companiesgroup_user',
        'users_status',
        'status_description',
        'password',
        'username'
    ];

    protected $hidden = [
        'password',
    ];
}