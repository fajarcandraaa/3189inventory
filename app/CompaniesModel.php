<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class CompaniesModel extends Model
{
    use SoftDeletes;
    protected $table = 'mstr_companies';
    protected $fillable = [
        'companies_name',
        'companies_address',
        'companies_code',
        'companies_phone'
    ];

    protected $hidden = [
        
    ];
}