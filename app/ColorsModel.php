<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
class ColorsModel extends Model
{
    protected $table = 'mstr_colors';

    protected $fillable = [
        'colors_name',
        'colors_code',
        'colors_description',
        'colors_initials',
        'colors_companiesgroup'
    ];

    protected $hidden = [
        
    ];
}